import React from 'react';

import Welcome from './Welcome';
import Service from './Service';
import Instructions from './Instructions';
import Rules from './Rules';

const Home = () => (
    <React.Fragment>
        <Welcome />
        <Service />
        <Instructions />
        <Rules />
    </React.Fragment>
);

export default Home;
