import React from 'react';
import {
   Switch,
   Route
} from 'react-router-dom';

import ScrollToTop from 'react-router-scroll-top';

import OurCompany from "./OurCompany";
import Home from "./Home";
import NotFound from './NotFound';
import Header from "./Header";
import Footer from "./Footer";
import TermsConditions from './TermsConditions';
import QuestionsAnswers from "./QuestionsAnswers";

export default class MainContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            content: false,
            clicked: null
        }
    }

    hideAndShow = clickedId => {
        this.setState(previousState =>
            ({
                content: !previousState.content,
                clicked: clickedId
            })
        )
    };

    render() {
        const { content, clicked } = this.state;
        return(
            <React.Fragment>
                <Header />
                <ScrollToTop>
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path='/home' component={Home} />
                        <Route path='/our-company' component={OurCompany} />
                        <Route path='/terms-conditions' component={TermsConditions} />
                        <Route
                            path='/questions-answers'
                            render={() => <QuestionsAnswers content={content} clicked={clicked} hideAndShow={this.hideAndShow} />}
                        />
                        <Route component={NotFound} />
                    </Switch>
                </ScrollToTop>
                <Footer />
            </React.Fragment>
        )
    }
}
