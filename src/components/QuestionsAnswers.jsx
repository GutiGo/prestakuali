import React from 'react';

import iconDown from '../../icons/down.png';

import style from './QuestionsAnswers.css';

import QuestionBlock from './QuestionBlock';
import questionsData from '../../src/questionsData.json';

const QuestionsAnswers = props => (
    <div className={style.questionsForm}>
        <div className={style.titleQuestions}>Preguntas Frecuentes</div>
        <div className={style.questionBlock}>
            <QuestionBlock
                hideAndShow={props.hideAndShow}
                data={questionsData}
                content={props.content}
                clicked={props.clicked}
                src={iconDown}
                alt={'Icon down'}
            />
        </div>
    </div>
);

export default QuestionsAnswers;
