import React from 'react';
import {
    Link
} from 'react-router-dom';

import logo from '../../img/logo.png';
import style from './Header.css';

const Header = () => (
    <div className={style.header}>
        <Link to='/home'><img className={style.logo} src={logo} alt='prestaKuali Logo' /></Link>
        <nav className={style.menuBar}>
            <Link className={style.menuButton} to='/home'>Inicio</Link>
            <Link className={style.menuButton} to='/our-company'>Nuestra empresa</Link>
        </nav>
    </div>
);

export default Header;
