import React from 'react';

import values from '../../img/values.png';

import style from './OurCompany.css';

const OurCompany = () => (
    <React.Fragment>
        <div className={style.formCompany}>
            <div className={style.welcomeCompany}>
                <div className={style.culture}>
                    <div className={style.ourCompany}>Nuestra empresa / Nuestra cultura y valores</div>
                    <br/>Nuestra cultura y valores nos ayudan a realizar nuestros propositos
                </div>
                <div className={style.vision}>
                    <div className={style.titleVision}>
                        Nuestra visión
                    </div>
                    <div className={style.designTitle}></div>
                    <p className={style.textVision}>
                        En el 2025 ser referente a nivel Latinoamérica en ofrecer soluciones
                        de financiamiento digitales, ágiles y transparentes promoviendo
                        la inclusión y la educación financiera.
                    </p>
                </div>
            </div>
            <div className={style.mapValues}>
                <img className={style.values} src={values} alt='Map values' />
            </div>
        </div>
    </React.Fragment>
);

export default OurCompany;
