import React from 'react';

import style from './TermsConditions.css';

const TermsConditions = () => (
    <div className={style.termsForm}>
        <div className={style.termsConditions}>
            <section>
                Términos y condiciones prestaKuali<br/>
                <br/>La sola utilización de la presente página web, sitio web o página de internet,
                cuyo nombre de dominio es www.prestakuali.com.mx, así como cualquier subdominio
                o página referenciada propiedad de Compañía de préstamos SARO, S.A.P.I. DE C.V. (“prestaKuali”),
                (conjuntamente el “Sitio”), le otorga al público en general la condición de “Usuario” e implica la
                aceptación, plena e incondicional, de todas y cada una de las condiciones generales y particulares
                incluidas en los presentes términos y condiciones (los “Términos y Condiciones”), en el momento mismo
                en que el Usuario acceda al Sitio. El Usuario, reconoce que el uso efectivo y/o registro exitoso de
                alguna operación de un Usuario, presume la aceptación de los Términos y Condiciones del Sitio, de
                conformidad con lo establecido en el Artículo 80 del Código de Comercio y en términos de lo dispuesto
                por el artículo 1803 del Código Civil Federal. En caso de no aceptar en forma absoluta y completa los
                Términos y Condiciones del presente Sitio, el Usuario deberá abstenerse de acceder, utilizar y observar
                el Sitio y en caso de que el Usuario acceda, utilice y observe el Sitio se considerará como una absoluta
                y expresa aceptación de los Términos y Condiciones aquí estipulados. Cualquier modificación a los Términos
                y Condiciones se entenderá por realizada cuando prestaKuali lo considere apropiado, siendo exclusiva
                responsabilidad del Usuario asegurarse de tomar conocimiento de tales modificaciones.
            </section>
            <section>
                <br/>1. Los servicios ofrecidos en el Sitio sólo están disponibles para aquellas personas que tengan
                capacidad legal para contratar. Por lo tanto, aquellos que no cumplan con esta condición deberán
                abstenerse de suministrar información personal para ser incluida en nuestras bases de datos y/o acceder al Sitio.<br/>
                <br/>2. El Usuario declara y garantiza, bajo protesta de decir verdad, que es mayor de edad y que cuenta con la
                capacidad jurídica necesaria para realizar las actividades que se contienen en el Sitio, así como que toda
                la información y documentación que ha proporcionado y/o proporcione en relación con cualquier actividad que
                se promueva en el Sitio, es verdadera, completa y correcta, quedando, por ende, obligada a indemnizar y sacar
                en paz y a salvo a prestaKuali de cualquier daño, perjuicio, demanda y/o acción que dicha omisión o falsedad le provoque.<br/>
                <br/>3. De igual forma, el Usuario hace constar que, tanto prestaKuali como cualquier tercero designado por dicha sociedad,
                podrá iniciar cualquier tipo de procedimiento contencioso en contra del Usuario, en caso de incumplimiento a cualquiera
                de los Términos y Condiciones del Sitio.<br/>
                <br/>4. El Usuario declara y garantiza que los recursos económicos utilizados en relación con las actividades que se promueven
                en el Sitio provienen y provendrán de fuentes lícitas y serán utilizados para actividades lícitas en todo momento,
                a sabiendas de que en caso contrario prestaKuali procederá a realizar todas las acciones a que haya lugar,
                quedando además obligado el Usuario a indemnizar y sacar en paz y a salvo a prestaKuali de cualquier daño,
                perjuicio, demanda y/o acción que dicho incumplimiento le provoque.<br/>
                <br/>5. El Usuario reconoce que cualquier actividad propuesta en el Sitio y/o los documentos que de ellas se generen,
                no son susceptibles de circular y por ende, ninguno de los documentos, actividades, obligaciones y derechos antes mencionados,
                versan sobre “valores” según se define en la Ley del Mercado de Valores, así como que prestaKuali no ha realizado ninguna solicitud,
                ofrecimiento o promoción para la captación, según la Ley de Instituciones de Crédito.<br/>
                <br/>6. El Usuario reconoce que prestaKuali solamente actuará, en su caso, como mandante para prestar los recursos del Usuario
                a nombre y por cuenta del mismo, a terceros interesados en obtener créditos. Por ende, cualquier monto que sea prestado
                a cualquier persona por virtud de las actividades contenidas en el Sitio, es a la exclusiva cuenta y riesgo del Usuario.
                Dichos recursos de ninguna forma y en ningún momento serán propiedad de prestaKuali. prestaKuali por ningún motivo y bajo
                ninguna causa o circunstancia será responsable, obligado solidario o de forma alguna responderá por el pago que deberán hacer
                los terceros que reciban recursos del Usuario por virtud de las actividades contenidas en el Sitio.<br/>
                <br/>7. De igual forma, el Usuario reconoce que, toda la información y/o cualquier análisis de riesgo, crédito, calificación
                de otros usuarios del Sitio y/o terceros interesados en obtener a crédito los recursos del Usuario, que haga prestaKuali
                e incluya en el Sitio y/o por cualquier otro medio se las haga conocer, son solamente estimados y para efectos informativos,
                por lo que prestaKuali de ninguna forma responde de la veracidad y/o certeza de la información presentada en el Sitio y/o de
                la información y documentación sometida por dichas personas y/o del análisis que prestaKuali haga de la misma.<br/>
                <br/>8. El Usuario reconoce que toda la información contenida en el Sitio, así como aquella que prestaKuali podrá entregarle,
                ya sea en forma escrita, electrónica o verbal, es información cuyo contenido el Usuario acuerda proteger con el carácter
                de confidencial (en adelante la “Información Confidencial”). La Información Confidencial estará marcada como tal y comprenderá,
                en forma enunciativa más no limitativa, correspondencia, información técnica, información de terceros y/o otros usuarios del Sitio,
                información comercial relativa a la organización y actividades del Sitio, conocimientos técnicos y contractuales de prestaKuali.
                Asimismo, el Usuario acuerda y reconoce que dentro de la Información Confidencial que le será entregada por prestaKuali,
                podrán existir secretos industriales, entendiendo por éstos toda aquella información propiedad de prestaKuali de aplicación
                industrial y comercial que le permite obtener y mantener, entre otras cosas, ventajas económicas y de mercado frente a sus
                competidores. El Usuario está de acuerdo y acepta conservar la Información Confidencial en estricta confidencialidad,
                y en este acto se obliga a no vender, divulgar, transferir, modificar, traducir, reproducir ni poner de otra
                forma Información Confidencial a disposición de terceros. El Usuario se obliga a poner el cuidado necesario en la protección
                de la Información Confidencial. Por otra parte, el Usuario acuerda y reconoce que las obligaciones de confidencialidad estarán
                vigentes durante todo el tiempo en que la Información Confidencial conserve dicho carácter. Asimismo, el Usuario reconoce que la
                divulgación no autorizada de la Información Confidencial, es castigada por la Ley de la Propiedad Industrial e incluso puede
                constituir la comisión de un delito. En caso de que el Usuario revele la Información Confidencial deberá indemnizar a prestaKuali
                de cualquier pérdida, daño, perjuicio, cargo o gastos (incluyendo honorarios de abogados) que resulten. prestaKuali tiene la
                convicción de proteger la Información Confidencial proporcionada por el Usuario y es el responsable de su tratamiento cuando
                sea recabada a través del Sitio.<br/>
                <br/>9. El Usuario sólo podrá imprimir y/o copiar cualquier información contenida o publicada en el Sitio exclusivamente para uso personal,
                quedando terminantemente prohibido el uso comercial de dicha información. En caso de ser persona moral se sujetará a lo dispuesto por el
                artículo 148, fracción IV de la Ley Federal del Derecho de Autor. La reimpresión, publicación, distribución, asignación, sublicencia,
                venta, reproducción electrónica o por otro medio, parcial o total, de cualquier información, documento o gráfico que aparezca en el Sitio,
                para cualquier uso distinto al personal no comercial le está expresamente prohibido al Usuario.<br/>
                <br/>10. prestaKuali se reserva el derecho de bloquear el acceso o remover en forma parcial o total toda información, comunicación o material
                que a su exclusivo juicio pueda resultar: i) abusivo, difamatorio u obsceno; ii) fraudulento, artificioso o engañoso; iii) violatorio de
                derechos de autor, marcas, confidencialidad, secretos industriales o cualquier derecho de propiedad intelectual de un tercero; iv)
                ofensivo o; v) que de cualquier forma contravenga lo establecido en los Términos y Condiciones.<br/>
                <br/>11. El Usuario reconoce que, al proporcionar la información de carácter personal requerida en alguno de los servicios que se prestan
                en este Sitio, otorgan a prestaKuali la autorización señalada en el artículo 109 de la Ley Federal del Derecho de Autor.<br/>
                <br/>12. El Sitio, los logotipos y todo el material que aparece en dicho Sitio, son marcas, nombres de dominio, nombres comerciales y
                obras artísticas propiedad de prestaKuali y/o sus respectivos titulares y están protegidos por los tratados internacionales y las
                leyes aplicables en materia de propiedad intelectual y derechos de autor. Los derechos de autor sobre el contenido, organización,
                recopilación, compilación, información, logotipos, fotografías, imágenes, programas, aplicaciones, y en general cualquier
                información contenida o publicada en el Sitio se encuentran debidamente protegidos a favor de prestaKuali, sus afiliados,
                proveedores y/o de sus respectivos propietarios, de conformidad con la legislación aplicable en materia de propiedad intelectual e
                industrial. Se prohíbe expresamente al Usuario modificar, alterar o suprimir, ya sea en forma total o parcial, los avisos, marcas,
                nombres comerciales, señas, anuncios, logotipos o en general cualquier indicación que se refiera a la propiedad de la información
                contenida en el Sitio. Por otra parte, el Usuario asume en este acto la obligación de notificar a prestaKuali, sobre cualquier
                uso o violación de parte de terceros de las marcas o cualesquiera otros derechos de propiedad industrial o intelectual propiedad
                de prestaKuali, tan pronto como el Usuario tenga conocimiento de dichas violaciones o usos indebidos. Lo anterior, en el claro
                entendido que el Usuario únicamente deberá notificar cualquier violación a los derechos de propiedad industrial o intelectual de
                prestaKuali, sin que le esté permitido defender o en cualquier forma actuar en nombre o representación de prestaKuali en la defensa
                de sus derechos de propiedad industrial o intelectual.<br/>
                <br/>13. En caso de que el Usuario transmita a prestaKuali cualquier información, programas, aplicaciones, software o en general cualquier
                material que requiera ser licenciado a través del Sitio, el usuario otorga con este acto a prestaKuali una licencia perpetua, universal,
                gratuita, no exclusiva, mundial y libre de regalías, que incluye los derechos de sublicenciar, vender, reproducir, distribuir, transmitir,
                crear trabajos derivados, exhibirlos y ejecutarlos públicamente. Lo establecido en el presente párrafo se aplicará igualmente a cualquier
                otra información que el Usuario envíe o transmita a prestaKuali por cualquier medio.<br/>
                <br/>14. En caso de que algún Usuario o tercero consideren que cualquiera de los contenidos que se encuentren o sean introducidos en dicho
                Sitio y/o cualquiera de sus servicios, violen sus derechos de propiedad intelectual deberán enviar una notificación por escrito a
                prestaKuali.<br/>
                <br/>15. El Usuario y el visitante del Sitio reconoce y acepta que prestaKuali podrá utilizar Cookies (las "Cookies"). Las Cookies son
                pequeños archivos que se instalan en el disco duro, con una duración limitada en el tiempo que ayudan a personalizar los servicios.
                El Sitio ofrece ciertas funcionalidades que sólo están disponibles mediante el empleo de Cookies.<br/>
                <br/>16. El Usuario y el visitante del Sitio reconoce y acepta que prestaKuali podrá utilizar Web Beacons. Un Web beacon es una
                imagen electrónica, también llamada single-pixel (1x1) o pixel transparente, que es colocada en código de una página Web.
                Un Web beacon tiene finalidades similares a las Cookies.<br/>
                <br/>17. prestaKuali realizará su mejor esfuerzo para mantener el Sitio operando correctamente y libre de virus electrónicos.
                No obstante lo anterior, prestaKuali por ningún motivo garantiza la ausencia de virus, errores, desactivadores o cualquier
                otro material contaminante o con funciones destructivas en la información o programas disponibles en o a través del Sitio y/o en general
                cualquier falla del Sitio, siendo por ende, la responsabilidad del Usurario el contar con los antivirus, firewalls y
                demás sistema de seguridad electrónica.<br/>
                <br/>18. El Usuario reconoce y acepta que prestaKuali podrá realizar el mantenimiento y cambios que considere adecuados o
                necesarios para el Sitio, por lo que el Sitio podrá estar inhábil o desactivado, sin responsabilidad alguna a cargo de prestaKuali,
                durante el tiempo en que se esté realizando dicho mantenimiento o cambio.<br/>
                <br/>19. prestaKuali podrá, en cualquier momento y cuando así lo estime conveniente, ceder total o parcialmente sus derechos y obligaciones
                derivados de los presentes Términos y Condiciones.<br/>
                <br/>20. Los presentes Términos y Condiciones estarán sujetos y serán interpretados de acuerdo con las leyes y ante los tribunales
                de Zapopan, Jalisco, México.<br/>
            </section>
        </div>
    </div>
);

export default TermsConditions;
