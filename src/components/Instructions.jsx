import React from 'react';

import door from '../../icons/door.png';
import form from '../../icons/form.png';
import wait from '../../icons/wait.png';
import money from '../../icons/money.png';
import transfer from '../../icons/transfer.png';
import idea from '../../icons/idea.png';

import style from './Instructions.css';

import SectionInstruction from "./SectionInstruction";

const Instructions = () => (
    <div className={style.formInstructions}>
        <div className={style.titleInstruction}>
            Cómo solicitar un préstamo rápido y fácil
        </div>
        <div className={style.firstSection}>
            <SectionInstruction
                number={'1'}
                src={door}
                alt={'Register'}
                label={'Regístrate'}
            />
            <SectionInstruction
                number={'2'}
                src={form}
                alt={'Form'}
                label={'Llena tu solicitud de crédito'}
            />
            <SectionInstruction
                number={'3'}
                src={wait}
                alt={'Wait'}
                label={'Espera a que tu solicitud sea aprobada y fondeada*'}
            />
            <SectionInstruction
                number={'4'}
                src={money}
                alt={'Money'}
                label={'Recibe el dinero en tu cuenta bancaria'}
            />
        </div>
        <div className={style.division} />
        <div className={style.titleInstruction}>
            Cómo invertir rápido y fácil
        </div>
        <div className={style.secondSection}>
            <SectionInstruction
                number={'1'}
                src={door}
                alt={'Register'}
                label={'Regístrate'}
            />
            <SectionInstruction
                number={'2'}
                src={transfer}
                alt={'Transfer'}
                label={'Deposita o tranfiere la cantidad que quieras' +
                'invertir en la cuenta concentradora de prestaKuali '}
            />
            <SectionInstruction
                number={'3'}
                src={idea}
                alt={'Idea'}
                label={'Escoge las solicitudes de crédito en' +
                'las cuales quieres invertir. Recuerda preguntar' +
                'todo lo necesario para estar seguro del perfil' +
                'del solicitante'}
            />
            <SectionInstruction
                number={'4'}
                src={money}
                alt={'Money'}
                label={'Envía tu selección y empieza' +
                'a ganar rendimientos de tu inversión'}
            />
        </div>
    </div>
);

export default Instructions;
