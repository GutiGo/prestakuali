import React from 'react';

import style from "./Welcome.css";
import tulum from '../../img/tulum.jpg';

const Welcome = () => (
    <React.Fragment>
        <img className={style.imageTulum} src={tulum} alt='Tulum' />
        <div className={style.welcome}>
             <span className={style.textWelcome}>
                 Te presentamos una nueva solución
                 para créditos e inversiones a menor costo.<br/>
                 <br />Seguridad, rapidez y atención
                 son la base de un buen préstamo.
             </span>
        </div>
        <div className={style.menuButtons}>
            <button className={style.button}>
                Pedir un prestamo
            </button>
            <button className={style.button}>
                Invertir
            </button>
        </div>
        <div className={style.extraButton}>
            <button className={style.lastButton}>
                ¿Ya tienes una cuenta?
            </button>
        </div>
    </React.Fragment>
);

export default Welcome;
