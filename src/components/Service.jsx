import React from 'react';

import style from './Service.css';
import connection from '../../icons/connection_people.png';
import creativity from '../../icons/creativity.png';
import lowPrice from '../../icons/low-price.png';

const Service = () => (
    <div className={style.formService}>
        <div className={style.title}>
            Nuestros Servicios
        </div>
        <div className={style.services}>
            <div className={style.service}>
                <img className={style.connection} src={connection} alt='People connection' />
                <div className={style.titleService}>
                        <span>
                            La mejor conexión<br/>
                            con gente
                        </span>
                </div>
                <div className={style.descriptionService}>
                        <span>
                            Conectamos gente con<br/>
                            necesidad de<br/>
                            un préstamo con<br/>
                            gente que quiere<br/>
                            invertir su dinero.
                        </span>
                </div>
            </div>
            <div className={style.service}>
                <img className={style.creativity} src={creativity} alt='Creativity' />
                <div className={style.titleService}>
                    <span>Diseño creativo</span>
                </div>
                <div className={style.descriptionService}>
                        <span>
                            Trabajamos todo de<br/>
                            manera digital con las<br/>
                            mejores tecnologías.<br/>
                            Creamos una plataforma<br/>
                            amigable e innovadora.
                        </span>
                </div>
            </div>
            <div className={style.service}>
                <img className={style.lowPrice} src={lowPrice} alt='Low price' />
                <div className={style.titleService}>
                    <span>Tasas bajas</span>
                </div>
                <div className={style.descriptionService}>
                        <span>
                            Ofrecemos créditos<br/>
                            e inversiones con<br/>
                            tasas mucho más<br/>
                            bajas que los bancos.
                        </span>
                </div>
            </div>
        </div>
    </div>
);

export default Service;
