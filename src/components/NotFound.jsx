import React from 'react';
import { Link } from 'react-router-dom';

import style from './NotFound.css';

const NotFound = () => (
    <React.Fragment>
        <div className={style.errorPage}>404 - Page Not Found</div>
        <div className={style.textErrorPage}>
            <p>Lo sentimos, parece ser que el contenido que estas buscando no existe.
                <Link className={style.backHome} to='/Home'> Haz Click aquí </Link>
                para regresar a la página de inicio.
            </p>
        </div>
    </React.Fragment>
);

export default NotFound;
