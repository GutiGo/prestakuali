import React from 'react';

import style from './Instructions.css';

const SectionInstruction = props => (
    <div className={style.instruction}>
        <div className={style.circle}>{props.number}</div>
        <img className={style.icon} src={props.src} alt={props.alt} />
        <div className={style.textInstruction}>
            {props.label}
        </div>
    </div>
);

export default SectionInstruction;
