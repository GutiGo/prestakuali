import React from 'react';

import style from './Rules.css';

const Rules = () => (
    <div className={style.ruleForm}>
        <div className={style.titleRule}>
            ¿Quieres tu primer préstamo?
        </div>
        <span className={style.rules}>
            ¿Tienes un excelente historial crediticio
            <br/>(mínimo de 3 años de experiencia en créditos de bancos
            u otras instituciones financieras)?
            <br/>¿Tienes una cuenta bancaria a tu nombre?
            <br/>¿Tienes entre 18 y 65 años?
            <br/>¿Tienes tus calificaciones de la universidad?
            <br/>¿Tienes tu comprobante de ingresos (estado de cuenta bancarios
            o recibos de nómina de los últimos 2 meses)?
            <br/>¿Tienes un reporte de ingresos y egresos?
        </span>
        <button className={style.loan}>
            SOLICITA TU CREDITO AHORA
        </button>
        <div className={[style.titleRule, style.division].join(' ')}>
            ¿Quieres tu primera inversión?
        </div>
        <span className={style.rules}>
            ¿Eres mayor de edad?
            <br/>¿Tienes una cuenta bancaria en México?
            <br/>¿Quieres invertir a partir de 300 pesos?
            <br/>¿Tienes una identificación oficial?
        </span>
        <button className={style.investment}>
            INVIERTE AHORA
        </button>
    </div>
);

export default Rules;
