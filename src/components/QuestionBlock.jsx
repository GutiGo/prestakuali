import React from 'react';

import style from "./QuestionsAnswers.css";

const QuestionBlock = props => (
    <React.Fragment>
        {props.data.length > 0 &&
            props.data.map(m => {
                return (
                    <div className={style.questionBlock} key={m.id}>
                        <button className={style.questionButton} onClick={() => props.hideAndShow(m.id)}>
                            <div>
                                {m.question}
                            </div>
                            <img className={style.iconDown} src={props.src} alt={props.alt} />
                        </button>
                        {(props.content && props.clicked === m.id) ?
                            <div className={style.answer}>
                                {m.answer}
                            </div>
                            :
                            null
                        }
                    </div>
                )
            })
        }
    </React.Fragment>
);

export default QuestionBlock;
