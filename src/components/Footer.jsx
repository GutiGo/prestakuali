import React from 'react';
import { Link } from 'react-router-dom';

import telephone from '../../icons/telephone.png';
import email from '../../icons/email.png';
import linkin from '../../icons/linkedin.png';
import facebook from '../../icons/facebook.png';
import twitter from '../../icons/twitter.png';
import chat from '../../icons/chat.png';

import style from './Footer.css';

const Footer = () => (
    <div className={style.footerComponent}>
        <div className={style.footerForm}>
            <div className={style.firstColumn}>
                <div className={style.title}>¿Necesitas ayuda?</div>
                <div className={style.contact}>
                    <img className={style.iconContact} src={telephone} alt='Telephone' />
                    <div>33 18 35 75 29</div>
                </div>
                <div className={style.contact}>
                    <img className={style.iconContact} src={email} alt='Email' />
                </div>
                <div className={style.textFooter}>
                    Estamos en proceso de certificación de la AFICO<br/>
                    <br/>En términos del artículo octavo transitorio de la ley para regular las instituciones
                    <br/>de tecnología financiera, la autorización para llevar a cabo operaciones dentro de
                    <br/>esta plataforma de colectivo se encuentra en trámite, por lo que la operación actual
                    <br/>de la plataforma financiamiento no es una actividad supervisadapor las autoridades mexicanas.
                </div>
            </div>
            <div className={style.secondColumn}>
                <div className={style.text}>
                    <Link className={style.footerLink} href="#" to='/questions-answers'>Preguntas frecuentes</Link>
                    <Link className={style.footerLink} to='/terms-conditions'>Términos y condiciones</Link>
                </div>
                <div className={style.socialNetworking}>
                    <img className={style.socialMedia} src={linkin} alt='LinkIn' />
                    <img className={style.socialMedia} src={facebook} alt='Facebook' />
                    <img className={style.socialMedia} src={twitter} alt='Twitter' />
                </div>
            </div>
        </div>
        <div className={style.lastInformation}>
            <div className={style.copyRight}>© 2019 prestaKuali</div>
            <button className={style.helpButton}>
                <div>Ayuda</div>
                <img className={style.iconHelp} src={chat} alt='Chat' />
            </button>
        </div>
    </div>
);

export default Footer;
