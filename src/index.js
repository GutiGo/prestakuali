import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import MainContainer from "./components/MainContainer";

ReactDOM.render(
    <BrowserRouter>
        <MainContainer />
    </BrowserRouter>, document.getElementById('app'),
);

module.hot.accept();
